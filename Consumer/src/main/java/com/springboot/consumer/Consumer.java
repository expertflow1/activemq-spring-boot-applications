package com.springboot.consumer;

import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @JmsListener(destination = "chat", subscription = "chat_subscription")
    public void receiveMessage(TextMessage message) throws JMSException {
        System.out.println("Received Message: "+ message.getText());
    }

}
