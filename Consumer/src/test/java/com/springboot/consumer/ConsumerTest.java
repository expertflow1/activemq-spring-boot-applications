package com.springboot.consumer;

import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Testcontainers
class ConsumerTest {

    @Container
    public static GenericContainer<?> activeMqContainer
            = new GenericContainer<>(DockerImageName.parse("rmohr/activemq")).withExposedPorts(61616);

    private final String message = "Hi";

    @SpyBean
    private final Consumer consumer;

    private JmsTemplate jmsTemplate;

    @Configuration
    @EnableJms
    static class TestConfiguration{
        @Bean
        public ActiveMQConnectionFactory connectionFactory(){
            return new ActiveMQConnectionFactory("tcp://localhost:"+activeMqContainer.getMappedPort(61616));
        }


        @Bean
        public JmsListenerContainerFactory<?> jmsListenerContainerFactory(){
            DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory());
            factory.setPubSubDomain(true);
            return factory;
        }

        @Bean
        public JmsTemplate jmsTemplate(){
            return new JmsTemplate(connectionFactory());
        }

    }

    @Autowired
    ConsumerTest(JmsTemplate jmsTemplate, Consumer consumer) {
        this.consumer = consumer;
        this.jmsTemplate = jmsTemplate;
    }


    @Test
    public void testThatConsumerReceivesCorrectMessage() throws JMSException {
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.send("chat", s->s.createTextMessage(this.message));
        ArgumentCaptor<TextMessage> messageCaptor = ArgumentCaptor.forClass(TextMessage.class);
        Mockito.verify(consumer, Mockito.timeout(1000)).receiveMessage(messageCaptor.capture());
    }
}