package com.springboot.producer;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    private String sentMessage;

    private JmsTemplate jmsTemplate;

    public Producer(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendMessage(String content) {
        jmsTemplate.send("chat", session -> session.createTextMessage(content));
        sentMessage = content;
        System.out.println("Sent Message: " + sentMessage);
    }

    public String getSentMessage() {
        return sentMessage;
    }
}
