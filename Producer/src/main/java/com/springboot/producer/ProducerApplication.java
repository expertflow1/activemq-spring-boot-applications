package com.springboot.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ProducerApplication {

    private static Producer producer;

    public ProducerApplication(Producer producer) {
        ProducerApplication.producer = producer;
    }

    public static void main(String[] args){

        SpringApplication.run(ProducerApplication.class, args);

        System.out.print("Write your message: ");
        String msg = new Scanner(System.in).nextLine();
        producer.sendMessage(msg);
    }

}
