package com.springboot.producer;

import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Testcontainers
class ProducerTest {

    @Container
    public static GenericContainer<?> activeMqContainer
            = new GenericContainer<>(DockerImageName.parse("rmohr/activemq")).withExposedPorts(61616);

    @SpyBean
    private final Producer producer;

    @SpyBean
    private final Listener listener;

    private JmsTemplate jmsTemplate;

    @Autowired
    ProducerTest(JmsTemplate jmsTemplate, Producer producer, Listener listener) {
        this.producer = producer;
        this.jmsTemplate = jmsTemplate;
        this.listener = listener;
    }

    @Configuration
    @EnableJms
    static class TestConfiguration{
        @Bean
        public ActiveMQConnectionFactory connectionFactory(){
            return new ActiveMQConnectionFactory("tcp://localhost:"+activeMqContainer.getMappedPort(61616));
        }

        @Bean
        public JmsListenerContainerFactory<?> jmsListenerContainerFactory(){
            DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory());
            factory.setPubSubDomain(true);
            return factory;
        }

        @Bean
        public JmsTemplate jmsTemplate(){
            JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory());
            jmsTemplate.setPubSubDomain(true);
            return jmsTemplate;
        }
    }

    @Component
    public static class Listener {

        @JmsListener(destination = "chat", subscription = "chat_subscription")
        public void receiveMessage(TextMessage message) throws JMSException {
            System.out.println("Received Message: "+ message.getText());
        }

    }

    @Test
    public void testThatProducerSendsAMessage() throws JMSException {
        String msg = "Hello";
        producer.sendMessage(msg);
        assertEquals("Hello", producer.getSentMessage());
        ArgumentCaptor<TextMessage> messageCaptor = ArgumentCaptor.forClass(TextMessage.class);
        Mockito.verify(listener, Mockito.timeout(1000)).receiveMessage(messageCaptor.capture());
        TextMessage receivedMessage = messageCaptor.getValue();
        assertEquals(msg, receivedMessage.getText());
    }
}